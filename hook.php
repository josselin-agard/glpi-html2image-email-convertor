<?php
/*
 -------------------------------------------------------------------------
 HTMLImageEmailConvertor plugin for GLPI
 Copyright (C) 2020 by the HTMLImageEmailConvertor Development Team.

 https://github.com/pluginsGLPI/htmlimageemailconvertor
 -------------------------------------------------------------------------

 LICENSE

 This file is part of HTMLImageEmailConvertor.

 HTMLImageEmailConvertor is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 HTMLImageEmailConvertor is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with HTMLImageEmailConvertor. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_htmlimageemailconvertor_install() {
    return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_htmlimageemailconvertor_uninstall() {
    return true;
}

/**
 * @param $queuedNotification
 */
function htmlimageemailconvertor_preadditem_called( $queuedNotification )
{
	global $CFG_GLPI ;
	static $is_first_time = true ;
	static $width = null ;
	static $height = null ;
	static $tag = null ;
	static $mails = array() ;
	
	/* 1 - Gets email's HTML body & open history file */
    $content = Html::entity_decode_deep( $queuedNotification -> input[ 'body_html' ] ) ; 
	$topic = $queuedNotification -> input[ 'name' ] ;
	
	if( $is_first_time OR ! in_array( $topic , $mails, true ) )
	{
		$is_first_time = false ;
		$mails[] = $topic ;
		$history_records = array() ;
		
		/* 2 - Initializes several useful paths */
		$htmlimage_email_convertor_pics_path = str_replace ( '/' , '\\' ,  HTMLIMAGEEMAILCONVERTOR_PICS_PATH ) . '\\' ;
		$tools = HTMLIMAGEEMAILCONVERTOR_DIR . '\\tools\\' ;
		$images_path = str_replace ( '/' , '\\' , GLPI_DOC_DIR . '\\JPEG\\htmltoimageconvertor\\' ) ;
		$images_html_path = $htmlimage_email_convertor_pics_path . 'html\\' ;
		$images_history_path = $htmlimage_email_convertor_pics_path . 'history\\' ;
		$history_file = fopen( $images_history_path . 'history-' . $queuedNotification -> input[ 'items_id' ] . '.txt' , 'a+' ) ;
		
		if( ! is_dir( $images_path ) )
			mkdir( $images_path , 0777, true ) ;
		
		if( ! is_dir( $images_html_path ) )
			mkdir( $images_html_path ) ;
		
			if( ! is_dir( $images_history_path ) )
		mkdir( $images_history_path ) ;
		
		
		/* 3 - Generates a random and unique HTML file name and creates the file */
		$html_file = $images_html_path . uniqid() . '.html' ;
		file_put_contents( $html_file , preg_replace( '#\\\n?#' , '' , $content ) );
		
		$ticket = new Ticket();
		if ( $ticket -> getFromDB( $queuedNotification -> input[ 'items_id' ] ) ) 
		{
			$users_id =  $ticket -> fields[ 'users_id_lastupdater' ] ;
		}
		

		/* 4 - Prepares the shell command and executes the HTML to Image converter tool */
		$cmd = escapeshellcmd( $tools . '\\python\\python.exe ' . $tools . 'html2image_convertor.py  "' . $html_file . '" ' . $htmlimage_email_convertor_pics_path . ' ' . $tools . 'wkhtmltopdf\\bin\\wkhtmltoimage.exe "' . $ticket -> getID() . '"' ) ;
		$image_name = exec( $cmd ) ;
		
		
		/* 5 - Moves the generated picture to GLPI's "files" folder and gets final image size */
		$image_name_moved = str_replace( '.jpeg' , '-no_resize.jpeg' , $image_name ) ;
		$ismoved = rename( $htmlimage_email_convertor_pics_path . $image_name , $images_path . $image_name_moved ) ;
		$image_name = $image_name_moved ;
		$img_infos = getimagesize($images_path . $image_name) ;
		$width = $img_infos[ 0 ] ;
		$height = $img_infos[ 1 ] ;
		unlink( $html_file ) ;
		
		/* 6 - Creates a Document object and adds it to DB in order to be able to use it as an embedded image in the email */
		$user_id = null ;
		
		$doc1_input = array
					(
						'name' => addslashes(sprintf(__('Document Ticket %d'), $queuedNotification -> input[ 'items_id' ] ) ) ,
						'tickets_id' => $queuedNotification -> input[ 'items_id' ] ,
						'mime' => 'image/jpeg' ,
						'filename' => $image_name ,
						'filepath' =>  'JPEG/htmltoimageconvertor/' . $image_name , 
						'sha1sum' => sha1_file( $images_path . $image_name ) ,
						'entities_id' => $queuedNotification -> input[ 'entities_id' ] ,
						'is_recursive' => 1,
						'users_id' => $users_id ,
						'documentcategories_id' => $CFG_GLPI["documentcategories_id_forticket"]
						
					) ;
						
		$doc1_item_input = array
					(
						'documents_id' => null ,
						'itemtype'     => $queuedNotification -> input[ 'itemtype' ] ,
						'items_id'     => $queuedNotification -> input[ 'items_id' ] ,
						'entities_id' => $queuedNotification -> input[ 'entities_id' ] ,
						'is_recursive' => 1 ,
						'users_id' => $users_id ,
						'documentcategories_id' => $CFG_GLPI["documentcategories_id_forticket"]
						
					) ;
					
					
		$document = new Document() ;
		$document_item = new Document_Item() ;
		
		$document -> check( -1 , CREATE , $doc1_input );

		if ( $ismoved AND $newID = $document -> add( $doc1_input ) ) 
		{
			$doc1_item_input[ 'documents_id' ] = $newID ; 
			
			$newIDDocItem = $document_item -> add( $doc1_item_input ) ;
			
			$tag = $document -> fields[ 'tag' ] ;
			
			$history_records[] = array( $images_path . $image_name , $newID , $newIDDocItem ) ;
			
			$image_name2 = str_replace( '-no_resize.jpeg' , 'b-no_resize.jpeg' , $image_name ) ;
			
			if( copy( $images_path . $image_name , $images_path . $image_name2 ) )
			{

				$document2 = new Document() ;
				$document_item2 = new Document_Item() ;
				
				$doc2_input = array
							(
								'name' => addslashes(sprintf(__('Document Ticket %d'), $queuedNotification -> input[ 'items_id' ] ) ) ,
								'tickets_id' => $queuedNotification -> input[ 'items_id' ] ,
								'mime' => 'image/jpeg' ,
								'filename' => $image_name2 ,
								'filepath' => 'JPEG/htmltoimageconvertor/' . $image_name2 , 
								'sha1sum' => sha1_file( $images_path . $image_name2 ) ,
								'entities_id' => $queuedNotification -> input[ 'entities_id' ] ,
								'is_recursive' => 1,
								'users_id' => $users_id ,
								'documentcategories_id' => $CFG_GLPI["documentcategories_id_forticket"]
								
							) ;
							
				$doc2_item_input = array
						(
							'documents_id' => null ,
							'itemtype'     => $queuedNotification -> input[ 'itemtype' ] ,
							'items_id'     => $queuedNotification -> input[ 'items_id' ] ,
							'entities_id' => $queuedNotification -> input[ 'entities_id' ] ,
							'is_recursive' => 1 ,
							'users_id' => $users_id ,
							'documentcategories_id' => $CFG_GLPI["documentcategories_id_forticket"]
							
						) ;
				
				$document2 -> check( -1 , CREATE , $doc2_input );

				if ( $newID2 = $document2 -> add( $doc2_input ) ) 
				{
					$doc2_item_input[ 'documents_id' ] = $newID2 ; 
					
					$newIDDocItem2 = $document_item2 -> add( $doc2_item_input ) ;
					
					$history_records[] = array( $images_path . $image_name2 , $newID2 , $newIDDocItem2 ) ;
				}
			}
		}
		
		unset( $ticket ) ;
		
		foreach( $history_records as $record )
			fputcsv( $history_file ,  $record ) ;
	
	}

	/* 7 - Generates the email's new HTML body to the QueuedNotification object */
	$html = "<html>
			<body>
				<div>
					<a href='http://sosinformatique/front/ticket.form.php?id=" . $queuedNotification -> input[ 'items_id' ] . "' target='_blank'>
						<img width='" . $width . "' height='" . $height . "' src='" . $tag . "' alt='image du ticket' />
						<!--<span style='display:inline-block; width:100%; text-align:center;'>Voir le ticket</span>-->
					</a>
				</div>
			</body>
		</html>" ;

	$queuedNotification -> input[ 'body_html' ] = str_replace( "'" , "\'" , $html ) ;

}
