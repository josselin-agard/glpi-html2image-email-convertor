<?php
/*
 -------------------------------------------------------------------------
 HTMLImageEmailConvertor plugin for GLPI
 Copyright (C) 2020 by the HTMLImageEmailConvertor Development Team.

 https://github.com/pluginsGLPI/htmlimageemailconvertor
 -------------------------------------------------------------------------

 LICENSE

 This file is part of HTMLImageEmailConvertor.

 HTMLImageEmailConvertor is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 HTMLImageEmailConvertor is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with HTMLImageEmailConvertor. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_HTMLIMAGEEMAILCONVERTOR_VERSION', '1.0.0');
// Minimal GLPI version, inclusive
define("PLUGIN_HTMLIMAGEEMAILCONVERTOR_MIN_GLPI", "9.4.5");

if (!defined("HTMLIMAGEEMAILCONVERTOR_DIR")) {
    define("HTMLIMAGEEMAILCONVERTOR_DIR", GLPI_ROOT . "\plugins\htmlimageemailconvertor");
}

if (!defined("HTMLIMAGEEMAILCONVERTOR_DOC_DIR")) {
    define("HTMLIMAGEEMAILCONVERTOR_DOC_DIR", GLPI_PLUGIN_DOC_DIR . "\htmlimageemailconvertor");
    if (!file_exists(HTMLIMAGEEMAILCONVERTOR_DOC_DIR)) {
        mkdir(HTMLIMAGEEMAILCONVERTOR_DOC_DIR);
    }
}

if (!defined("HTMLIMAGEEMAILCONVERTOR_PICS_PATH")) {
    define("HTMLIMAGEEMAILCONVERTOR_PICS_PATH", HTMLIMAGEEMAILCONVERTOR_DOC_DIR . "\pics");
    if (!file_exists(HTMLIMAGEEMAILCONVERTOR_PICS_PATH)) {
        mkdir(HTMLIMAGEEMAILCONVERTOR_PICS_PATH);
    }
}

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_htmlimageemailconvertor() {
    global $PLUGIN_HOOKS;

    $PLUGIN_HOOKS['csrf_compliant']['htmlimageemailconvertor'] = true;
    $PLUGIN_HOOKS['pre_item_add']['htmlimageemailconvertor'] =
        [
            'QueuedNotification' => 'htmlimageemailconvertor_preadditem_called'
        ];
}


/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_htmlimageemailconvertor() {
    return [
        'name'           => 'HTMLImageEmailConvertor',
        'version'        => PLUGIN_HTMLIMAGEEMAILCONVERTOR_VERSION,
        'author'         => 'Josselin et Olivier Solfi',
        'license'        => 'GPL V3+',
        'homepage'       => 'N/A',
        'requirements'   => [
            'glpi' => [
                'min' => PLUGIN_HTMLIMAGEEMAILCONVERTOR_MIN_GLPI,
            ]
        ]
    ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_htmlimageemailconvertor_check_prerequisites() {

    //Version check is not done by core in GLPI < 9.2 but has to be delegated to core in GLPI >= 9.2.
    $version = preg_replace('/^((\d+\.?)+).*$/', '$1', GLPI_VERSION);
    if (version_compare($version, PLUGIN_HTMLIMAGEEMAILCONVERTOR_MIN_GLPI, '<')) {
        echo "This plugin requires GLPI >= " . PLUGIN_HTMLIMAGEEMAILCONVERTOR_MIN_GLPI ;
        return false;
    }
    return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_htmlimageemailconvertor_check_config($verbose = false) {
    if (true) { // Your configuration check
        return true;
    }

    if ($verbose) {
        echo __('Installed / not configured', 'htmlimageemailconvertor');
    }
    return false;
}
