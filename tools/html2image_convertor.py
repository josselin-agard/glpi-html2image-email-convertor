#HTML2Image Email Convertor

import sys
import secrets
import imgkit

html_file = sys.argv[ 1 ]
path = sys.argv[ 2 ]
wkhtmltopdf_path = sys.argv[ 3 ] 
ticketid = sys.argv[ 4 ]


options = {
    'quiet': '',
    'format': 'jpeg',
    'quality': 100
}
try:
    config = imgkit.config(wkhtmltoimage=wkhtmltopdf_path)

    out_file_name = 'img-ticket-' + ticketid + '-' + secrets.token_urlsafe( 6 )  + '.jpeg'
    out_file_name_path = path + out_file_name

    imgkit.from_file( html_file , out_file_name_path , options = options , config = config )
except:
    print( sys.exc_info() )

print( out_file_name )